FROM python:3.6-alpine

RUN adduser -D denver_dualsport

WORKDIR /home/denver_dualsport

COPY requirements.txt requirements.txt
RUN python -m venv venv
RUN venv/bin/pip install -r requirements.txt
RUN venv/bin/pip install gunicorn

COPY app app

ENV FLASK_APP app

USER denver_dualsport

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]